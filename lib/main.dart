import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Welcome to Flutter',
      theme: new ThemeData(
        scaffoldBackgroundColor: const Color(0x000),
      ),
      home: Scaffold(
          body: Center(
        child: Text(
          'Hello Flutter',
          style: TextStyle(
              fontSize: 34,
              color: Colors.deepOrange,
              fontWeight: FontWeight.bold),
        ),
      )),
    );
  }
}
